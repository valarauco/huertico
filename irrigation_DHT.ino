#include <Wire.h>
//Lib in https://github.com/Seeed-Studio/OLED_Display_128X64
#include <SeeedOLED.h>
#include "DHT.h"

// set the DHT Pin
#define DHTPIN 7

// initialize the library with the numbers of the interface pins
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);  

#define Solenoide 8
int moistureSensor = 1;

void setup() {

  Wire.begin();
  SeeedOled.init();  //initialze SEEED OLED display
  DDRB|=0x21;        
  PORTB |= 0x21;

  SeeedOled.clearDisplay();          //clear the screen and set start position to top left corner
  SeeedOled.setNormalDisplay();      //Set display to normal mode (i.e non-inverse mode)
  SeeedOled.setPageMode();           //Set addressing mode to Page Mode
  SeeedOled.setTextXY(0,0);          //Set the cursor to Xth Page, Yth Column  
  SeeedOled.putString("H3ll0 W0rld! #_#"); //Print the String         //

  // Serial Begin so we can see the data from the moisture sensor in our serial input window. 
  Serial.begin(9600);

  // Setup all the Arduino Pins
  // Solenoid valve must be plugged to normal close. 
  pinMode(Solenoide, OUTPUT);

  //Turn OFF any power to the Relay channels
  digitalWrite(Solenoide,LOW);
  delay(2000); //Wait 2 seconds before starting sequence

}

void loop() {
  delay(500);
  // read humidity
  float h = dht.readHumidity();
  //read temperature in Celsius
  float t = dht.readTemperature(false);
  SeeedOled.setTextXY(2,0);
  SeeedOled.putString("Humidity: ");
  SeeedOled.putFloat(h);
  SeeedOled.setTextXY(3,0);
  SeeedOled.putString("Temp *C: ");
  SeeedOled.putFloat(t);
  
  // read the input on analog pin 0:
  int sensorValue = analogRead(moistureSensor);
  SeeedOled.setTextXY(4,0);
  SeeedOled.putString("Moisture: ");
  SeeedOled.putFloat(sensorValue);

  // Se mide el valor de ausencia de agua (valor de sequedad).
  if (sensorValue >= 550)  {
    digitalWrite(Solenoide, LOW);
  }

  else {
    digitalWrite(Solenoide, HIGH);
  }

}
