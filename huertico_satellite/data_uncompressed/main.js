var mdata = [];

loadMoisture();
//loadLight();
loadConfig();

function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Label', 'Value'],
        ['Moisture', (100-(mdata*100/700))],
        ['Light', 10]
    ]);

    var options = {
        width: 300, height: 150,
        redFrom: 75, redTo: 100,
        yellowFrom:50, yellowTo: 75,
        minorTicks: 10
    };

    var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

    chart.draw(data, options);
}

function loadMoisture() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            mdata = this.responseText;
            google.charts.load('current', { 'packages': ['gauge'] });
            google.charts.setOnLoadCallback(drawChart);
        }
    };
    xmlhttp.open("GET", "readMoisture.do", true);
    xmlhttp.send();
}

function loadConfig() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var cfgdata = JSON.parse(this.responseText);
            if (cfgdata){
              document.getElementById("server").value = cfgdata.server;
              if (cfgdata.powersave == 1){
                document.getElementById("powersave").disabled = false ;
              } else {
                document.getElementById("powersave").disabled = true ;
              }
              document.getElementById("sleeptime").value = cfgdata.sleeptime;
            }
        }
    };
    xmlhttp.open("GET", "readConfig.do", true);
    xmlhttp.send();
}


document.getElementById("refresh").onclick = function() {
    loadMoisture();
    //loadLight();
}

document.body.onresize = drawChart;
