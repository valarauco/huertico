#define MQTT_DEBUG
#include <ESP8266WiFi.h>    //https://github.com/esp8266/Arduino
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <WiFiManager.h>   //https://github.com/tzapu/WiFiManager
#include <FS.h>            //https://github.com/esp8266/arduino-esp8266fs-plugin
#include <ArduinoJson.h>   //https://github.com/bblanchon/ArduinoJson/
#include "Adafruit_MQTT.h" //https://github.com/adafruit/Adafruit_MQTT_Library/
#include "Adafruit_MQTT_Client.h"
#include <DHT.h>
// Adafruit DHT library needs a hack: https://github.com/adafruit/DHT-sensor-library/issues/48


// SENSOR Vcc GPIO //
#define moisture_vcc 2
#define light_vcc 3
#define dht_vcc 4

// SENSOR Data read GPIO //
#define moisture_sensor A0
#define light_sensor A0
#define dht_sensor 5

// Huertico Satellite Names and Topics //
const String sat_name = "Huertico_" + String(ESP.getChipId(), HEX); // Huertico Satellite Name
const String mdns_name = "huertico" + String(ESP.getChipId(), HEX); // Domain name for the mDNS responder
const String moisture_topic = "huertico/moisture";  // ToDo: add ChipID to get data from diferente Hueticos
const String light_topic = "huertico/light";
const String humidity_topic = "huertico/humidity";
const String temperature_topic = "huertico/temperature";

// SENSOR Data values //
int moisture_value, light_value;
float humidity_value, temperature_value;



// Satellite Configs //
String mqtt_server = "huerticomqtt.local";
int mqtt_port = 1883;
int sleep_time = 60000; // sleep 1 minute
bool shouldSaveConfig = false;

// Objects init //
ESP8266WebServer server = ESP8266WebServer(80);
File fs_upload_file;
DHT dht(dht_sensor, DHT11);
WiFiClient mqtt_wclient;  // Create an ESP8266 WiFiClient class to connect to the MQTT server.
//WiFiClientSecure mqtt_wclient; // or... use WiFiFlientSecure for SSL
Adafruit_MQTT_Client* mqtt;


/*__________________________________________________________MAIN_FUNCTIONS__________________________________________________________*/

void setup() {
  Serial.begin(115200);        // Start the Serial communication to send messages to the computer
  delay(10);
  Serial.println("\r\n");

  //setup pinmode
  pinMode(moisture_vcc, OUTPUT);
  pinMode(light_vcc, OUTPUT);
  pinMode(dht_vcc, OUTPUT);
  pinMode(A0, INPUT); //Analog for light and moisture
  dht.begin();       // setup DHT

  startSPIFFS();
  startWiFi();
  startMQTT();
  // if no MQTT start Server
  if (! mqtt || ! mqtt->connected()) {
    startMDNS();
    startServer();
  }
}

void loop() {
  if (! MQTTConnect()) {
    server.handleClient(); // run the server if no MQTT
  } else {
    readSensors();
    ProcessMQTT();
  }
  //
  Serial.println("Wait 10 min");
  delay(600000);
}

/*__________________________________________________________SETUP_FUNCTIONS__________________________________________________________*/

void startSPIFFS() { // Start the SPIFFS and list all contents
  SPIFFS.begin();                             // Start the SPI Flash File System (SPIFFS)
  Serial.println("SPIFFS started. Contents:");
  {
    Dir dir = SPIFFS.openDir("/");
    while (dir.next()) {                      // List the file system contents
      String fileName = dir.fileName();
      size_t fileSize = dir.fileSize();
      Serial.printf("\tFS File: %s, size: %s\r\n", fileName.c_str(), formatBytes(fileSize).c_str());
    }
    Serial.printf("\n");
  }
}

void startWiFi() {
  Serial.println("Starting WiFi");
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset saved settings
  //wifiManager.resetSettings();
  //disable debug on serial console
  //wifiManager.setDebugOutput(false);

  loadConfigs();

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  // Set config parameters for Huertico Satallite
  WiFiManagerParameter custom_header("<p><strong>Huertico Settings</strong></p>");
  WiFiManagerParameter custom_lbl_mqtt_server("<label for=\"server\">MQTT server</label>");
  WiFiManagerParameter custom_mqtt_server("server", "Huertico MQTT server", toChar(mqtt_server), 40);
  WiFiManagerParameter custom_lbl_mqtt_port("<br /><br /><label for=\"port\">MQTT server port (default: 1883)</label>");
  WiFiManagerParameter custom_mqtt_port("port", "Huertico MQTT port", toChar(String(mqtt_port)), 6);
  WiFiManagerParameter custom_lbl_sleep_time("<br /><br /><label for=\"sleep\">Sleep time</label>");
  WiFiManagerParameter custom_sleep_time("sleep", "Sleep time", toChar(String(sleep_time)), 20);
  wifiManager.addParameter(&custom_header);
  wifiManager.addParameter(&custom_lbl_mqtt_server);
  wifiManager.addParameter(&custom_mqtt_server);
  wifiManager.addParameter(&custom_lbl_mqtt_port);
  wifiManager.addParameter(&custom_mqtt_port);
  wifiManager.addParameter(&custom_lbl_sleep_time);
  wifiManager.addParameter(&custom_sleep_time);

  //fetches ssid and pass from eeprom and tries to connect
  //if it does not connect it starts an access point with the specified sat_name
  //and goes into a blocking loop awaiting configuration
  wifiManager.autoConnect(toChar(sat_name));
  //or use this for auto generated name ESP + ChipID
  //wifiManager.autoConnect();

  //read updated parameters
  mqtt_server = custom_mqtt_server.getValue();
  mqtt_port = String(custom_mqtt_port.getValue()).toInt();
  sleep_time = String(custom_sleep_time.getValue()).toInt();

  if (shouldSaveConfig) {
    saveConfigs();
  }
}

void startMQTT() {
  if (loadConfigs()) {
    mqtt = new Adafruit_MQTT_Client(&mqtt_wclient, mqtt_server.c_str(), mqtt_port);
    //mqtt = new Adafruit_MQTT_Client(&mqtt_wclient, toChar(mqtt_server), mqtt_port); //Dunno why this isn't working
    if (MQTTConnect()) {
      if (! mqtt->ping()) {
        Serial.println("No ping from MQTT, disconnect!");
        mqtt->disconnect();
      }
    }
  }
}


void startMDNS() {  // Start the mDNS responder
  MDNS.begin(toChar(mdns_name));                     // start the multicast domain name server
  Serial.print("mDNS responder started: http://");
  Serial.print(mdns_name);
  Serial.println(".local");
}

void startServer() { // Start a HTTP server with a file read handler and an upload handler
  server.on("/edit.html",  HTTP_POST, []() {  // If a POST request is sent to the /edit.html address,
    server.send(200, "text/plain", "");
  }, handleFileUpload);                       // go to 'handleFileUpload'

  server.on("/",  HTTP_POST, handleMainSave); // If a POST request is sent to the / address, go to 'handleMainSave'

  server.onNotFound(handleNotFound);          // if someone requests any other file or page, go to function 'handleNotFound'
  // and check if the file exists

  server.begin();                             // start the HTTP server
  Serial.println("HTTP server started.");
}


/*__________________________________________________________SERVER_HANDLERS__________________________________________________________*/

void handleNotFound() { // if the requested file or page doesn't exist, return a 404 not found error
  if (!handleFileRead(server.uri(), true)) {        // check if the file exists in the flash memory (SPIFFS), if so, send it
    server.send(404, "text/plain", "404: File Not Found");
  }
}

bool handleFileRead(String path, bool protect) { // send the right file to the client (if it exists)
  Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) path += "index.html";          // If a folder is requested, send the index file
  if (path.endsWith(".do")) return handleAction(path);   // Handle Satellite actions
  if (path.endsWith(".conf") && protect) return false;   // Protect configs
  String contentType = getContentType(path);             // Get the MIME type
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) { // If the file exists, either as a compressed archive, or normal
    if (SPIFFS.exists(pathWithGz))                         // If there's a compressed version available
      path += ".gz";                                         // Use the compressed verion
    File file = SPIFFS.open(path, "r");                    // Open the file
    size_t sent = server.streamFile(file, contentType);    // Send it to the client
    file.close();                                          // Close the file again
    Serial.println(String("\tSent file: ") + path);
    return true;
  }
  Serial.println(String("\tFile Not Found: ") + path);   // If the file doesn't exist, return false
  return false;
}

bool handleAction(String path) {
  String action = path.substring(1, path.indexOf(".do"));
  Serial.println(String("\tSent action: ") + action);
  /*if (action.equals("readMoisture")){
    read_moisture();
    server.send(200, "text/plain", String(moisture_value));
    return true;
    }*/
  if (action.equals("readConfig")) {
    return handleFileRead("/.huertico.conf", false);
  }
  return false;
}

void handleFileUpload() { // upload a new file to the SPIFFS
  HTTPUpload& upload = server.upload();
  String path;
  if (upload.status == UPLOAD_FILE_START) {
    path = upload.filename;
    if (!path.startsWith("/")) path = "/" + path;
    if (!path.endsWith(".gz")) {                         // The file server always prefers a compressed version of a file
      String pathWithGz = path + ".gz";                  // So if an uploaded file is not compressed, the existing compressed
      if (SPIFFS.exists(pathWithGz))                     // version of that file must be deleted (if it exists)
        SPIFFS.remove(pathWithGz);
    }
    Serial.print("handleFileUpload Name: "); Serial.println(path);
    fs_upload_file = SPIFFS.open(path, "w");               // Open the file for writing in SPIFFS (create if it doesn't exist)
    path = String();
  } else if (upload.status == UPLOAD_FILE_WRITE) {
    if (fs_upload_file)
      fs_upload_file.write(upload.buf, upload.currentSize); // Write the received bytes to the file
  } else if (upload.status == UPLOAD_FILE_END) {
    if (fs_upload_file) {                                   // If the file was successfully created
      fs_upload_file.close();                               // Close the file again
      Serial.print("handleFileUpload Size: "); Serial.println(upload.totalSize);
      server.sendHeader("Location", "/success.html");     // Redirect the client to the success page
      server.send(303);
    } else {
      server.send(500, "text/plain", "500: couldn't create file");
    }
  }
}

void handleMainSave() { // save configuration data to the SPIFFS
  //Fix check data (sanitaze)
  String mqtt = server.arg("server");
  boolean powersave = server.arg("powersave") == "powersave" ? true : false;
  int sleeptime = server.arg("sleeptime").toInt();

  String path = ".huertico.json";
  DynamicJsonBuffer jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["server"] = mqtt;
  root["powersave"] = powersave;
  root["sleeptime"] = sleeptime;

  File fsConfFile = SPIFFS.open("/.huertico.conf", "w");
  if (!fsConfFile) {
    Serial.println("Failed to open config file for writing");
    server.send(500, "text/plain", "500: couldn't create file");
  } else {
    Serial.println("Saving Configs");
    root.printTo(fsConfFile);
    fs_upload_file.close();
    server.sendHeader("Location", "/success.html");     // Redirect the client to the success page
    server.send(303);
  }

}

/*__________________________________________________________SENSORS_FUNCTIONS__________________________________________________________*/
void sensorsOff() {
  // power-down sensors
  digitalWrite(moisture_vcc, LOW);
  digitalWrite(light_vcc, LOW);
  digitalWrite(dht_vcc, LOW);
}

void readSensors() {
  sensorsOff();
  delay(100);

  digitalWrite(dht_vcc, HIGH);         // power up sensor
  delay(2500);
  humidity_value = dht.readHumidity(); // read humidity
  temperature_value = dht.readTemperature(); //read temperature
  // Check if any reads failed
  if (isnan(humidity_value) || isnan(temperature_value)) {
    Serial.println("Failed to read from DHT sensor!");
  } /*else {
    Serial.print("humidity: ");
    Serial.println(humidity_value);
    Serial.print("temperature: ");
    Serial.println(temperature_value);
  }*/
  digitalWrite(dht_vcc, LOW);         // power down sensor

  digitalWrite(moisture_vcc, HIGH);            // power up sensor
  delay(100);
  moisture_value = analogRead(moisture_sensor);
  //moisture_value =  (moisture_value * 100) / 1024; // read moisture
  //Serial.print("Moisture: ");
  //Serial.println(moisture_value);
  digitalWrite(moisture_vcc,LOW);              //power down sensor
  
  digitalWrite(light_vcc,HIGH);          // power up sensor
  delay(100);
  light_value = analogRead(light_sensor); // read light
  light_value = 1024 - light_value; // read light
  //Serial.print("light: ");
  //Serial.println(light_value);
  //digitalWrite(light_vcc,LOW);           //power down sensor
 
  sensorsOff();
}

/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/

void ProcessMQTT() {
  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).
  MQTTConnect();

  // Now we can publish stuff!
  Adafruit_MQTT_Publish moisture_pub = Adafruit_MQTT_Publish(mqtt, moisture_topic.c_str());
  Adafruit_MQTT_Publish light_pub = Adafruit_MQTT_Publish(mqtt, light_topic.c_str());
  Adafruit_MQTT_Publish humidity_pub = Adafruit_MQTT_Publish(mqtt, humidity_topic.c_str());
  Adafruit_MQTT_Publish temperature_pub = Adafruit_MQTT_Publish(mqtt, temperature_topic.c_str());
  Serial.println("Publishing sensors data");
  if (!moisture_pub.publish(moisture_value)) {
    Serial.println("Error publishing moisture data");
  }
  if (!light_pub.publish(light_value)) {
    Serial.println("Error publishing light data");
  }
  if (isnan(humidity_value)) {
    if (!humidity_pub.publish(humidity_value)) {
      Serial.println("Error publishing humidity data");
    }

  }
  if (isnan(temperature_value)) {
    if (!temperature_pub.publish(temperature_value)) {
      Serial.println("Error publishing temperature data");
    }
  }

  // ping the server to keep the mqtt connection alive
  // NOT required if you are publishing once every KEEPALIVE seconds
  if (! mqtt->ping()) {
    mqtt->disconnect();
  }
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.
bool MQTTConnect() {
  // Stop if no MQTT
  if (! mqtt) {
    return false;
  }
  // Stop if already connected.
  if (mqtt->connected()) {
    return true;
  }

  Serial.print("Connecting to MQTT... ");

  int8_t ret;
  uint8_t retries = 3;
  while ((ret = mqtt->connect()) != 0 && retries > 0) { // connect will return 0 for connected
    Serial.println(mqtt->connectErrorString(ret));
    Serial.println("Retrying MQTT connection in 5 seconds...");
    mqtt->disconnect();
    delay(5000);  // wait 5 seconds
    retries--;
  }

  if (mqtt->connected()) {
    Serial.println("MQTT Connected!");
    return true;
  }
  Serial.println("MQTT connection failed!");
  return false;
}

/*__________________________________________________________HELPER_FUNCTIONS__________________________________________________________*/

String formatBytes(size_t bytes) { // convert sizes in bytes to KB and MB
  if (bytes < 1024) {
    return String(bytes) + "B";
  } else if (bytes < (1024 * 1024)) {
    return String(bytes / 1024.0) + "KB";
  } else if (bytes < (1024 * 1024 * 1024)) {
    return String(bytes / 1024.0 / 1024.0) + "MB";
  }
}

String getContentType(String filename) { // determine the filetype of a given filename, based on the extension
  if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

const char * toChar(String s) {
  return s.c_str();
}

void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

bool loadConfigs() {
  bool result = true;
  String config_path = "/.huertico.conf";
  if (SPIFFS.exists(config_path)) {
    //file exists, reading and loading
    Serial.println("reading config file");
    File conf_file = SPIFFS.open(config_path, "r");
    if (conf_file) {
      Serial.println("opened config file");
      size_t fsize = conf_file.size();
      // Allocate a buffer to store contents of the file.
      std::unique_ptr<char[]> buf(new char[fsize]);

      conf_file.readBytes(buf.get(), fsize);
      DynamicJsonBuffer jsonBuffer;
      JsonObject& json = jsonBuffer.parseObject(buf.get());
      json.printTo(Serial);
      Serial.println("");
      if (json.success()) {
        Serial.println("\nparsed json");
        mqtt_server = json["mqtt_server"].asString();
        mqtt_port = json["mqtt_port"];
        sleep_time = json["sleep_time"];
      } else {
        Serial.println("failed to load json config");
        result = false;
      }
      conf_file.close();
    }
  } else {
    Serial.println("No json config");
    result = false;
  }

  return result;
}

bool saveConfigs() {
  //TODO: Fix check config data (sanitaze)
  String config_path = "/.huertico.conf";
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
  json["mqtt_server"] = mqtt_server;
  json["mqtt_port"] = mqtt_port;
  json["sleep_time"] = sleep_time;

  File conf_file = SPIFFS.open(config_path, "w");
  if (!conf_file) {
    Serial.println("failed to open config file for writing");
    return false;
  }
  json.printTo(Serial);
  Serial.println("");
  json.printTo(conf_file);
  conf_file.close();
  return true;
}

