#!/bin/bash

[[ -d data_uncompressed ]] || { echo "error: no data_uncompressed"
				 exit 1
				}
[[ -d data ]] || mkdir data

for f in $( ls data_uncompressed/ ); do
  echo "Gzipping ${f}"
  gzip -c "data_uncompressed/${f}" > "data/${f}.gz"
done

echo "Done"
exit 0
