// Huertico v0.2
//

#include <Wire.h>
//Lib in https://github.com/Seeed-Studio/OLED_Display_128X64
#include <SeeedOLED.h>

#define Solenoide 8
#define sensorVcc 9
#define moistureSensor 1
#define lowMoisture 550
#define highMoisture 450

int moistureValue = 0;


void setup() {
  
  Wire.begin();
  SeeedOled.init();  //initialze SEEED OLED display
  DDRB|=0x21;        
  PORTB |= 0x21;

  SeeedOled.clearDisplay();          //clear the screen and set start position to top left corner
  SeeedOled.setNormalDisplay();      //Set display to normal mode (i.e non-inverse mode)
  SeeedOled.setPageMode();           //Set addressing mode to Page Mode
  SeeedOled.setTextXY(0,0);          //Set the cursor to Xth Page, Yth Column  
  SeeedOled.putString("H3ll0 W0rld! #_#"); //Print the String         //

  // Serial Begin so we can see the data from the moisture sensor in our serial input window. 
  Serial.begin(9600);

  
  // Setup all the Arduino Pins
  // Solenoid valve must be plugged to normal close. 
  pinMode(Solenoide, OUTPUT);
  // Power up sensor only when needed
  pinMode(sensorVcc, OUTPUT);

  //Turn OFF any power to the Relay channels and sensors
  digitalWrite(Solenoide,LOW);
  digitalWrite(sensorVcc,LOW);
  delay(2000); //Wait 2 seconds before starting sequence
}

void loop() {
  read_sensors();
  update_oled();
  
  // In our sensor moistureValue actually means dryness 
  // (check your moisture sensor to know what type is it)
  if (moistureValue >= lowMoisture)  {
    // start watering
    watering_loop();
  }
  digitalWrite(Solenoide, LOW);
  delay(1800000); //wait 30 minutes until next check
}

void update_oled(){
  SeeedOled.setTextXY(2,0);
  SeeedOled.putString("Moisture:     ");
  SeeedOled.setTextXY(2,10);
  SeeedOled.putNumber(moistureValue);
}

void read_sensors(){
  // power-up sensors
  digitalWrite(sensorVcc,HIGH);
  delay(100);
  
  // read moisture
  moistureValue = analogRead(moistureSensor);
  
  // power-down sensors
  digitalWrite(sensorVcc,LOW);
}

void watering_loop(){
  // Turn on relay
  digitalWrite(Solenoide, HIGH);
  
  // Water until highMoisture is reached
  boolean stop_watering = false;
  while (!stop_watering){
    delay(1000);
    read_sensors();
    update_oled();
    if (moistureValue < highMoisture)  {
      stop_watering = true;
    }
  }
  digitalWrite(Solenoide, LOW);
}

