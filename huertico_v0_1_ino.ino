// Huertico v0.1
// Primera implementacion funcional

// definicion de constantes
#define Solenoide 8
#define moistureSensor 1

void setup() {
  // Setup all the Arduino Pins
  // Solenoid valve must be plugged to normal open. 
  pinMode(Solenoide, OUTPUT);

  //Turn OFF any power to the Relay channels
  digitalWrite(Solenoide,LOW);
  delay(2000); //Wait 2 seconds before starting sequence
}

void loop() {
  delay(1000);
  // read the input on analog pin 0:
  int sensorValue = analogRead(moistureSensor);
  // Se mide el valor de ausencia de agua (valor de sequedad).
  if (sensorValue >= 550)  {
    digitalWrite(Solenoide, LOW);
    delay(1800000); //wait 30 minutes to next check
  }

  else {
    digitalWrite(Solenoide, HIGH);
  }
}


