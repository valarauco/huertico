#define CH1 8
int moistureSensor = 0;

void setup() {

  // Serial Begin so we can see the data from the moisture sensor in our serial input window. 
  Serial.begin(9600);

  // Setup all the Arduino Pins
  // Solenoid valve must be plugged to normal close. 
  pinMode(CH1, OUTPUT);

  //Turn OFF any power to the Relay channels
  digitalWrite(CH1,LOW);
  delay(2000); //Wait 2 seconds before starting sequence

}

void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(moistureSensor);

  // Se mide el valor de ausencia de agua (valor de sequedad).
  if (sensorValue >= 550)  {
    digitalWrite(CH1, HIGH);
  }

  else {
    digitalWrite(CH1, LOW);
  }

}
