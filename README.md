# Huertico
El robot que siembra.

Proyecto de hardware y software abierto para automatizar un jardín para interiores y exteriores.

## Notas
- Hay diferentes sensores de humedad, unos miden cantidad de humedad y otros ausencia de humedad. Se debe revisar el sensor antes de colocarlo.
- El sensor de humedad se degrada muy rápido por lo que lo mejor es energizarlo solamente cuando se va a hacer la lectura conectandolo en un pin digital.
- Los umbrales de irigación (cuanta humedad para iniciar o detener irrigación) parecen ser dependientes de la precisión del sensor y el terreno donde se está desplegando por lo que deben monitorearse y ajustarse según el caso
