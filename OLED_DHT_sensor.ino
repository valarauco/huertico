#include <Wire.h>
//Lib in https://github.com/Seeed-Studio/OLED_Display_128X64
#include <SeeedOLED.h>
#include "DHT.h"

// set the DHT Pin
#define DHTPIN 8

// initialize the library with the numbers of the interface pins
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);  

void setup()
{
  Wire.begin();
  SeeedOled.init();  //initialze SEEED OLED display
  DDRB|=0x21;        
  PORTB |= 0x21;

  SeeedOled.clearDisplay();          //clear the screen and set start position to top left corner
  SeeedOled.setNormalDisplay();      //Set display to normal mode (i.e non-inverse mode)
  SeeedOled.setPageMode();           //Set addressing mode to Page Mode
  SeeedOled.setTextXY(0,0);          //Set the cursor to Xth Page, Yth Column  
  SeeedOled.putString("H3ll0 W0rld! #_#"); //Print the String         //


}

void loop()
{
  delay(500);
  // read humidity
  float h = dht.readHumidity();
  //read temperature in Celsius
  float t = dht.readTemperature(false);
  SeeedOled.setTextXY(2,0);
  SeeedOled.putString("Humidity: ");
  SeeedOled.putFloat(h);
  SeeedOled.setTextXY(3,0);
  SeeedOled.putString("Temp *C: ");
  SeeedOled.putFloat(t);
  
}

